# Introduction

This repository defines a standard virtual machine (Ubuntu Linux 14.04 server)
which has all the packages required to develop and package Crystal RIP (and
soon Picocolour).

# Install

## Get dependencies

VirtualBox is used to run your virtual machine. Vagrant configures the virtual
machine through Virtualbox.

You need to install:

- [VirtualBox 4.2](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](http://www.vagrantup.com/downloads)
- (windows only) [Git for Windows](https://git-scm.com/download/win)

*Windows Only* Open your new application called **Git Bash**. This is a command
shell similar to what you would see on Linux.

## Create (provision) your Virtual Machine

First download this repository as it defines the virtual machine in the
`Vagrantfile`

```
git clone https://bitbucket.org/jfmachines/virtual-machine
```

Then change directory into `virtual-machine`.

```
cd virtual-machine
```

Then instruct Vagrant to create the Virtual Machine based on the `Vagrantfile`

```
vagrant up
```

## Log in to your new virtual machine

```
vagrant ssh
```

## Clone Crystal RIP and Picocolour repositories

```
git clone https://bitbucket.org/jfmachines/crystal-rip
git clone https://bitbucket.org/jfmachines/picocolour
```

**NOTE:** Make sure you review the `README` files of both repositories before
starting work on them, in case there are special instructions there.

# Move files between Host machine and VM

Your host machine `virtual-machine/` directory is shared with `/vagrant/`
inside the guest machine. You can use the `/vagrant/shared_folder/` directory
for moving files (eg release packages) out of the virtual machine.

# Reset Virtual Machine

Warning: this completely destroys your virtual machine and everything in it.

```
vagrant destroy
vagrant up
```

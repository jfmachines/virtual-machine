#!/bin/bash -eux

install_wine_ppa() {
    sudo add-apt-repository -y ppa:ubuntu-wine/ppa
}

add_i386_architecture_for_wine() {
    sudo dpkg --add-architecture i386
}

update_package_index() {
    sudo apt-get update
}

upgrade_packages() {
    sudo apt-get upgrade -y
}

install_required_packages() {
    sudo apt-get install -y pyqt4-dev-tools xvfb git tree zip unzip mkisofs wine
}


install_wine_ppa
add_i386_architecture_for_wine
update_package_index
upgrade_packages
install_required_packages
